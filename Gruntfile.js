module.exports = function( grunt ) {

  grunt.initConfig({
    //ad dependencias são adicionadas em formato de json... desta maneira por exemplo
    uglify : {
      options : {
        mangle : false
      },

      my_target : {
        files : {
          'assets/js/main.js' : [ 'assets/_js/scripts.js' ] //colocar as dependencias aqui na ordem que devem ser minificadas
        }
      }
    } // uglify

	
    sass : {
      dist : {
        options : { style : 'compressed' },
        files : {
          'assets/css/style.css' : 'assets/_sass/style.sass'
        }
      }
    } // sass
	
	watch : {
      dist : {
        files : [
          'assets/_js/**/*',
          'assets/_sass/**/*'
        ],

        tasks : [ 'uglify', 'sass' ]
      }
    } // watch
	
  });


  // Plugins do Grunt
  grunt.loadNpmTasks( 'grunt-contrib-uglify' );
  grunt.loadNpmTasks( 'grunt-contrib-sass' );
  grunt.loadNpmTasks( 'grunt-contrib-watch' );
  
  // Tarefas que serão executadas
  grunt.registerTask( 'default', [ 'uglify', 'sass'] );
  
  // Tarefas para watch
  grunt.registerTask('w', ['watch']);

};